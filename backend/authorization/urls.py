from django.urls import path

from . import views

urlpatterns = [
    path(route=r'registration/', view=views.RegistrationView.as_view(), name="registration"),
    path(route=r'login/', view=views.LoginView.as_view(), name="login"),
    path(route=r"reset_password", view=views.ResetPasswordView.as_view(), name="reset_password"),
]
