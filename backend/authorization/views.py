from django.views.decorators.debug import sensitive_variables

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import (
    CreateModelMixin
)

from .serializers import RegistrationSerializer, LoginSerializer, ResetPasswordSerializer


class RegistrationView(CreateModelMixin, GenericAPIView):
    """
    Registration end point.
    """
    serializer_class = RegistrationSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, args, kwargs)


class LoginView(APIView):
    """
    Login end point.
    """
    serializer_class = LoginSerializer

    @sensitive_variables('token')
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        token, username, user_id = serializer.save()
        return Response(
            data=dict(
                token=token.key,
                username=username,
                id=user_id
            ),
            status=status.HTTP_200_OK
        )


class ResetPasswordView(GenericAPIView,
                        CreateModelMixin):
    """
    Reset Password end point.
    """
    serializer_class = ResetPasswordSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
