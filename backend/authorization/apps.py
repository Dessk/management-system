from django.apps import AppConfig


class AuthorizationConfig(AppConfig):
    "Config authorization applications."
    name = 'authorization'
