from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _

from pycountry import countries


class AbstractUser(AbstractUser):
    """
    Base model for Users instances.
    """
    COUNTRY_CHOICES = [(country.alpha_2, _(country.name)) for country in countries]
    MALE_SEX = 'M'
    FEMALE_SEX = 'F'
    SEX_CHOICES = [
        (MALE_SEX, _('Male')),
        (FEMALE_SEX, _('Female'))
    ]
    sex = models.CharField(
        verbose_name=_('sex'),
        max_length=20,
        choices=SEX_CHOICES,
        default=MALE_SEX
    )
    country = models.CharField(
        max_length=200,
        choices=COUNTRY_CHOICES,
        verbose_name=_('country'),
        default=COUNTRY_CHOICES[79]
    )
    first_name = models.CharField(
        verbose_name=_('first name'),
        max_length=30,
    )
    last_name = models.CharField(
        verbose_name=_('last name'),
        max_length=150,
    )
    email = models.EmailField(
        verbose_name=_('email address'),
        unique=True,
        error_messages={
            'unique': _("A user with that email already exists."),
        },
    )
    pesel = models.CharField(
        verbose_name=_('pesel'),
        unique=True,
        max_length=11,
        error_messages={
            'unique': _("A user with that pesel already exists.")
        }
    )
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name']

    class Meta:
        ordering = ['date_joined']
        verbose_name = _("user")
        verbose_name_plural = _('users')
        abstract = True


class User(AbstractUser):
    """
    Users within the Django authentication system are represented by this
    model.
    Username, password, email, first_name, last_name are required. Other fields are optional.
    """

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'
