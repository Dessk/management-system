from typing import Dict
from random import sample
from string import ascii_uppercase, digits, punctuation

from django.contrib.auth import authenticate
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.html import strip_tags
from django.utils.translation import gettext_lazy as _

from rest_framework import serializers
from rest_framework.authtoken.models import Token

from .models import User


class RegistrationSerializer(serializers.ModelSerializer):
    """
    Serializer for registration.
    """
    password = serializers.CharField(
        write_only=True,
        required=True,
        trim_whitespace=False,
        min_length=11,
        style={
            'input_type': 'password',
        }
    )
    password_confirm = serializers.CharField(
        write_only=True,
        required=True,
        trim_whitespace=False,
        min_length=11,
        style={
            'input_type': 'password',
        }
    )

    class Meta:
        model = User
        fields = [
            'sex', 'country',
            'first_name', 'last_name',
            'email', 'password',
            'password_confirm', "pesel",
            'username'
        ]

    def validate_first_name(self, first_name: str) -> str:
        """
        Method retuns correct first name.
        Arguments:
            first_name {str} -- first name value
        Returns:
            str -- first name in format 'Janek'
        """
        return first_name.capitalize()

    def validate_last_name(self, last_name: str) -> str:
        """
        Method return correct last name
        Arguments:
            last_name {str} -- last name value
        Returns:
            str -- last name in format 'Kowalski'
        """
        return last_name.capitalize()

    def validate(self, validated_data: Dict[str, str]) -> Dict[str, str]:
        """
        Method checks if the given password matches.
        Arguments:
            validated_data {Dict[str, str]} -- all data for the first valdiation
        Raises:
            serializers.ValidationError: if two password don't match raise exception
        Returns:
            Dict[str, str] -- all validation data with correct password
        """
        password = validated_data.pop('password', None)
        password_confirm = validated_data.get('password_confirm')
        if password and password_confirm and password_confirm != password:
            raise serializers.ValidationError(
                detail=_("Two password do not match."),
                code="mismatch"
            )
        return validated_data

    def save(self) -> User:
        """
        Method sets password for new User.
        Next, saves in database.
        Returns:
            User -- new User instance
        """
        password = self.validated_data.pop("password_confirm")
        new_user = User(**self.validated_data)
        new_user.set_password(password)
        new_user.save()
        return new_user


class LoginSerializer(serializers.Serializer):
    """
    Serializer for Login.
    """
    pesel = serializers.CharField()
    password = serializers.CharField(
        write_only=True,
        required=True,
        trim_whitespace=False,
        style={
            'input_type': 'password',
        }
    )
    class Meta:
        fields = ["pesel", "password"]


    def validate(self, validate_data: Dict[str, str]) -> Dict[str, str]:
        """
        The method checks the data in the last step validation.
        The method tries to authenticate in with the given password and pesel.
        Arguments:
            validate_data {dictionary} --
                dictionary with data that has undergone the first validation
        Raises:
            serializers.ValidationError:
                Method raises error when user don't exist.
        Returns:
            Dict[str, str] -- All correct data.
        """
        pesel = validate_data.get("pesel")
        password = validate_data.get("password")
        if pesel and password:
            user = authenticate(
                pesel=pesel,
                password=password
            )
        if not user:
            msg = _('Unable to log in with provided credentials.')
            raise serializers.ValidationError(msg, code='authorization')
        validate_data["user"] = user
        return validate_data

    def save(self):
        """
        Method saves user in database and return token.
        """
        user = self.validated_data["user"]
        user.last_login = timezone.now()
        user.save()
        return Token.objects.get_or_create(user=user)[0], user.username, user.id


class ResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()
    pesel = serializers.CharField()

    def get_new_password(self, password_length=10):
        """
        The method returns a new random user password.
        Keyword Arguments:
            password_length {int} -- password length (default: {10})
        Returns:
            new_password {string} --
                The method returns a new password which consists:
                    of capital letters
                    of two numbers and
                    two special characters
        """
        numbers = ''.join(sample(digits, 2))
        special_sign = ''.join(sample(punctuation, 2))
        rest_password = ''.join(sample(ascii_uppercase, password_length - 4))
        return rest_password + numbers + special_sign

    def validate(self, validate_data):
        """
        The method checks the data in the last step validation.
        The method tries to find the user with the given username and email
        Arguments:
            validate_data {dictionary} --
                dictionary with data that has undergone the first validation
        Raises:
            serializers.ValidationError:
                Method raises error when user don't exist.
        Returns:
            validate data {dictionary} -- All correct data.
        """
        email = validate_data.get("email")
        pesel = validate_data.get("pesel")
        if email and pesel:
            try:
                user = User.objects.get(
                    email=email,
                    pesel=pesel
                )
            except ObjectDoesNotExist:
                raise serializers.ValidationError(
                    detail=_("User about this email and pesel doesn't exists."),
                    code="bad_user"
                )
            else:
                validate_data["user"] = user
        return validate_data

    def save(self):
        """
        Method sets new password and send email for user with new password.
        """
        user = self.validated_data["user"]
        new_password = self.get_new_password()
        user.set_password(new_password)
        user.save()
        html_message = render_to_string(
            template_name="messages/reset_password.html",
            context={
                "username": user.username,
                "new_password": new_password
            }
        )
        user.email_user(
            subject="Reset password.",
            message=strip_tags(html_message),
            from_email="Managenent system administration.",
            html_message=html_message
        )
