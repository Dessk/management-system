from typing import Type

from django.contrib.auth.backends import ModelBackend

from .models import User


class PeselAuthBackend(ModelBackend):
    """
    New authentication system based on login, pesel number and password.
    """

    def authenticate(self, request, pesel: str, password: str, **kwargs) -> Type[User]:
        """
        Method authenticates user used pesel and password.
        Arguments:
            request  -- request
            pesel {str} -- pesel for user
            password {str} -- password for user
        Returns:
            Type[User] -- if user about this password and number pesel exsits return User instance
                          otherwise None
        """
        try:
            user = User.objects.get(pesel=pesel)
        except User.DoesNotExist:
            pass
        else:
            if user.check_password(password) and self.user_can_authenticate(user):
                return user
