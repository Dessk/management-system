from datetime import date

from authorization.models import User
from rest_framework.reverse import reverse


def get_user(**kwargs):
    """
        Method create and get user.
    Arguments:
        You can enter new data to create a new user. For example:
            username {str}="your_username"
            email {str}="your@email.gmail"
            first_name {str}="Yourname"
            last_name {str}="Yourlastname"
            pesel="pesel"
        If you do not want to provide data default user has:
            username="tester",
            email="tester@gmail.com",
            first_name="Tester",
            last_name="Last Name",
            pesel="96031208830"
    Returns:
        User instance
    """
    user_data = kwargs if kwargs else dict(
        username="tester",
        email="tester@gmail.com",
        first_name="Tester",
        last_name="Last Name",
        pesel="96031208830"
    )
    return User(**user_data)


def login_user(client, user):
    """
    Login user in current session.
    """
    token = client.post(
        path=reverse(viewname="login"),
        data=dict(
            pesel=user.pesel,
            password="Password123.,"
        )
    ).data["token"]
    client.credentials(HTTP_AUTHORIZATION='Token ' + str(token))


def logout_user(client):
    """
    Logout user.
    """
    client.credentials()
