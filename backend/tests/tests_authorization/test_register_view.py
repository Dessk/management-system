import pytest

from rest_framework.reverse import reverse

from authorization.models import User


@pytest.fixture
def registration_data():
    return {
        "username": "tester",
        "email": "test@gmail.com",
        "password": "Password12.,",
        "password_confirm": "Password12.,",
        "first_name": "John",
        "last_name": "Kowalski",
        "pesel": "96031208830",
        "country": "PL",
        "sex": "M"
    }


@pytest.mark.django_db
def test_registration_view_when_data_are_correct(registration_data, api_client):
    response = api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    user = User.objects.get(pesel=registration_data["pesel"])
    assert response.status_code == 201
    assert user.pesel == registration_data["pesel"]
    assert user.first_name == registration_data["first_name"]
    assert user.last_name == registration_data["last_name"]
    assert user.username == registration_data["username"]
    assert user.email == registration_data["email"]
    assert user.sex == registration_data["sex"]
    assert user.country == registration_data["country"]


@pytest.mark.django_db
def test_registration_view_when_username_field_is_empty(registration_data, api_client):
    del registration_data['username']
    response = api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    assert response.status_code == 400
    assert response.data['username'][0] == 'This field is required.'


@pytest.mark.django_db
def test_registration_view_when_user_exists_about_this_username(registration_data, api_client):
    api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    response = api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    assert response.status_code == 400
    assert response.data['username'][0] == "A user with that username already exists."


@pytest.mark.django_db
def test_registration_view_when_email_field_is_empty(registration_data, api_client):
    del registration_data['email']
    response = api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    assert response.status_code == 400
    assert response.data['email'][0] == 'This field is required.'


@pytest.mark.django_db
def test_registration_view_when_user_exists_about_this_email(registration_data, api_client):
    api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    registration_data["username"] = "NewUser"
    response = api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    assert response.status_code == 400
    assert response.data['email'][0] == "A user with that email already exists."


@pytest.mark.django_db
def test_registration_view_when_password_and_password_confirm_do_not_match(registration_data, api_client):
    registration_data["password"] = "PassworDoNotMatch12.m,,"
    response = api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    assert response.status_code == 400
    assert response.data["non_field_errors"][0] == "Two password do not match."


@pytest.mark.django_db
def test_registration_view_when_first_name_field_has_different_letter_sizes(registration_data, api_client):
    registration_data["first_name"] = "pRzEmYsLaw"
    response = api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    assert response.status_code == 201
    assert response.data['first_name'] == "Przemyslaw"


@pytest.mark.django_db
def test_registration_view_when_last_name_field_has_different_letter_sizes(registration_data, api_client):
    registration_data["last_name"] = "RozYcKi"
    response = api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    assert response.status_code == 201
    assert response.data['last_name'] == "Rozycki"


@pytest.mark.django_db
def test_registration_view_when_pesel_field_is_empty(registration_data, api_client):
    del registration_data['pesel']
    response = api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    assert response.status_code == 400
    assert response.data['pesel'][0] == 'This field is required.'


@pytest.mark.django_db
def test_registration_view_when_user_exists_about_this_pesel(registration_data, api_client):
    api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    registration_data["username"] = "NewUser"
    registration_data["email"] = "admin_test@gmail.com"
    response = api_client.post(
        path=reverse("registration"),
        data=registration_data
    )
    assert response.status_code == 400
    assert response.data['pesel'][0] == "A user with that pesel already exists."
