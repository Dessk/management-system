import pytest
from django.contrib.auth.hashers import check_password

from rest_framework.test import APITestCase
from rest_framework.reverse import reverse

from authorization.models import User


@pytest.fixture
def reset_password_data(user):
    return {
        'pesel': user.pesel,
        'email': user.email
    }


@pytest.fixture
def old_password():
    return "Password123.,"


@pytest.mark.django_db
def test_reset_password_view_when_data_are_correct(api_client, reset_password_data, old_password):
    assert check_password(
        password=old_password,
        encoded=User.objects.get(
            pesel=reset_password_data["pesel"]
        ).password
    ) == True
    response = api_client.post(
        path=reverse('reset_password'),
        data=reset_password_data
    )
    assert response.status_code == 201
    assert check_password(
        password=old_password,
        encoded=User.objects.get(
            pesel=reset_password_data["pesel"]
        ).password
    ) == False


@pytest.mark.django_db
def test_reset_password_view_when_given_pesel_is_incorrect(api_client, reset_password_data):
    reset_password_data['pesel'] = 'bad_pesel'
    response = api_client.post(
        path=reverse('reset_password'),
        data=reset_password_data
    )
    assert response.status_code == 400
    assert response.data["non_field_errors"][0] == "User about this email and pesel doesn't exists."


@pytest.mark.django_db
def test_reset_password_view_when_given_email_is_incorrect(api_client, reset_password_data):
    reset_password_data["email"] = "bad_email@gmail.com"
    response = api_client.post(
        path=reverse('reset_password'),
        data=reset_password_data
    )
    assert response.status_code == 400
    assert response.data["non_field_errors"][0] == "User about this email and pesel doesn't exists."
