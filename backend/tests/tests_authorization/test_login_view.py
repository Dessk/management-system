import pytest

from rest_framework.authtoken.models import Token
from rest_framework.reverse import reverse


@pytest.fixture
def login_data(user):
    return {
        "pesel": user.pesel,
        "password": "Password123.,"
    }


@pytest.mark.django_db
def test_login_view_when_data_are_correct(login_data, api_client, user):
    response = api_client.post(
        path=reverse("login"),
        data=login_data
    )
    token = Token.objects.get_or_create(user=user)[0].key
    assert response.data == {
        "token": token,
        "username": "tester",
        "id": 1
    }
    assert response.status_code == 200


@pytest.mark.django_db
@pytest.mark.parametrize('bad_pesel, expected_status_code, expected_msg',[
    ('212131', 400, 'Unable to log in with provided credentials.'),
    ('2324W561', 400, 'Unable to log in with provided credentials.'),
    ('2123e563', 400, 'Unable to log in with provided credentials.'),
    ('4324131555', 400, 'Unable to log in with provided credentials.'),
])
def test_login_view_when_pesel_is_incorrect(bad_pesel, expected_status_code, 
                                            expected_msg, login_data, api_client):
    login_data['pesel'] = bad_pesel
    response = api_client.post(
        path=reverse("login"),
        data=login_data
    )
    assert response.status_code == expected_status_code
    assert response.data["non_field_errors"][0] == expected_msg


@pytest.mark.django_db
@pytest.mark.parametrize('bad_password, expected_status_code, expected_msg',[
    ('212131', 400, 'Unable to log in with provided credentials.'),
    ('2324W561', 400, 'Unable to log in with provided credentials.'),
    ('2123e563', 400, 'Unable to log in with provided credentials.'),
    ('4324131555', 400, 'Unable to log in with provided credentials.'),
])
def test_login_view_when_password_is_incorrect(bad_password, expected_status_code, 
                                            expected_msg, login_data, api_client):
    login_data['password'] = bad_password
    response = api_client.post(
        path=reverse("login"),
        data=login_data
    )
    assert response.status_code == expected_status_code
    assert response.data["non_field_errors"][0] == expected_msg
