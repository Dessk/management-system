import pytest

from rest_framework.test import APIClient

from .helpful_functions import get_user


@pytest.fixture
def api_client():
    """
    Api client for tests.
    """
    return APIClient()


@pytest.mark.django_db
@pytest.fixture
def user():
    """
    User for tests.
    """
    user = get_user()
    user.set_password("Password123.,")
    user.save()
    return  user
