import pytest

from rest_framework.reverse import reverse

from ..helpful_functions import login_user, logout_user


@pytest.mark.django_db
def test_roles_view_create_when_data_are_correct_and_user_is_authenticated(user, api_client, create_role_data):
    login_user(client=api_client, user=user)
    response = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    assert response.status_code == 201
    assert response.data == {
        "name": "test_create_role",
        "id": response.data["id"]
    }


@pytest.mark.django_db
def test_roles_view_create_when_data_are_correct_and_user_is_not_authenticated(api_client, create_role_data):
    response = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    assert response.status_code == 401
    assert response.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_roles_view_create_when_role_name_exsits_in_database(
    api_client, user, create_role_data
):
    login_user(client=api_client, user=user)
    for _ in range(2):
        response = api_client.post(
            path=reverse("roles-list"),
            data=create_role_data
        )
    assert response.status_code == 400
    assert response.data["name"][0] == 'roles with this name already exists.'


@pytest.mark.django_db
def test_roles_view_update_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data, update_role_data
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_update = api_client.put(
        path=reverse("roles-detail", args=[response_create.data["id"]]),
        data=update_role_data
    )
    assert response_update.status_code == 200
    assert response_update.data == {
        "name": "test_update_role",
        "id": response_create.data["id"]
    }


@pytest.mark.django_db
def test_roles_view_update_when_data_are_correct_and_user_is_not_authenticated(
    api_client, user, create_role_data, update_role_data
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    logout_user(client=api_client)
    response_update = api_client.put(
        path=reverse("roles-detail", args=[response_create.data["id"]]),
        data=update_role_data
    )
    assert response_update.status_code == 401
    assert response_update.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_roles_view_update_role_when_name_role_exists_in_database_and_user_is_authenticated(
    user, api_client, create_role_data
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    create_role_data.update(name="second_test_role")
    api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_update = api_client.put(
        path=reverse("roles-detail", args=[response_create.data["id"]]),
        data={"name": create_role_data["name"]}
    )
    assert response_update.status_code == 400
    assert response_update.data["name"][0] == 'roles with this name already exists.'


@pytest.mark.django_db
def test_roles_view_delete_when_role_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_delete = api_client.delete(
        path=reverse("roles-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 204
    assert response_delete.data is None


@pytest.mark.django_db
def test_roles_view_delete_when_data_are_correct_and_user_is_not_authenticated(
    api_client, user, create_role_data
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    logout_user(client=api_client)
    response_delete = api_client.delete(
        path=reverse("roles-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 401
    assert response_delete.data["detail"] == "Authentication credentials were not provided."
