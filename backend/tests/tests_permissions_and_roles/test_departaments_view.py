import pytest

from rest_framework.reverse import reverse

from ..helpful_functions import login_user, logout_user


@pytest.mark.django_db
def test_departaments_view_create_when_data_are_correct_and_user_is_authenticated(
    user, api_client, name_departament
):
    login_user(client=api_client, user=user)
    response = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    assert response.status_code == 201
    assert response.data == {
        "name": name_departament,
        "chief_id": user.id,
        "id": 1
    }


@pytest.mark.django_db
def test_departaments_view_create_when_data_are_correct_and_user_is_not_authenticated(
    user, api_client, name_departament):
    response = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    assert response.status_code == 401
    assert response.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_departaments_view_when_name_departament_is_duplicated(
    api_client, user, name_departament
):
    login_user(client=api_client, user=user)
    for _ in range(2):
        response = api_client.post(
            path=reverse("departaments-list"),
            data={
                "name": name_departament,
                "chief_id": user.id
            }
        )
    assert response.status_code == 400
    assert response.data['name'][0] == 'departaments with this name already exists.'


@pytest.mark.django_db
def test_departaments_view_update_when_data_are_correct_and_user_is_authenticated(
    user, api_client, name_departament
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    response_update = api_client.put(
        path=reverse("departaments-detail", args=[response_create.data["id"]]),
        data={
            "name": "update_department",
            "chief_id": user.id
        }
    )
    assert response_update.status_code == 200
    assert response_update.data == {
        "id": response_create.data["id"],
        "name": "update_department",
        "chief_id": user.id
    }


@pytest.mark.django_db
def test_departaments_view_update_when_data_are_correct_and_user_is_not_authenticated(
    api_client, user, name_departament
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    logout_user(client=api_client)
    response_update = api_client.put(
        path=reverse("departaments-detail", args=[response_create.data["id"]]),
        data={
            "name": "update_department",
            "chief_id": user.id
        }
    )
    assert response_update.status_code == 401
    assert response_update.data["detail"] == "Authentication credentials were not provided."

@pytest.mark.django_db
def test_departaments_view_delete_when_data_are_correct_and_user_is_authenticated(
    user, api_client, name_departament
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    response_delete = api_client.delete(
        path=reverse("departaments-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 204
    assert response_delete.data is None


@pytest.mark.django_db
def test_departaments_view_delete_when_data_are_correct_and_user_is_not_authenticated(
    api_client, user, name_departament
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    logout_user(client=api_client)
    response_delete = api_client.delete(
        path=reverse("departaments-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 401
    assert response_delete.data["detail"] == "Authentication credentials were not provided."
