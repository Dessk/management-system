import pytest

from rest_framework.reverse import reverse

from ..helpful_functions import login_user, logout_user


@pytest.mark.django_db
def test_permissions_view_create_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data, create_application_data, name_permission
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_application = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data,
    )
    response = api_client.post(
        path=reverse("permissions-list"),
        data={
            "role_id": response_role.data["id"],
            "application_id": response_application.data["id"],
            "name": name_permission
        }
    )
    assert response.status_code == 201
    assert response.data == {
        "role_id": response_role.data["id"],
        "application_id": response_application.data["id"],
        "name": name_permission,
        "id": 1
    }


@pytest.mark.django_db
def test_permissions_view_create_when_data_are_correct_and_user_is_not_authenticated(
    user, api_client, create_role_data, create_application_data, name_permission
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_application = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data,
    )
    logout_user(client=api_client)
    response = api_client.post(
        path=reverse("permissions-list"),
        data={
            "role_id": response_role.data["id"],
            "application_id": response_application.data["id"],
            "name": name_permission
        }
    )
    assert response.status_code == 401
    assert response.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_permissions_view_create_when_name_is_not_unique_in_database(
    user, api_client, create_role_data, create_application_data, name_permission
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_application = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data,
    )
    for _ in range(2):
        response = api_client.post(
            path=reverse("permissions-list"),
            data={
                "role_id": response_role.data["id"],
                "application_id": response_application.data["id"],
                "name": name_permission
            }
        )
    assert response.status_code == 400
    assert response.data['name'][0] == 'permissions with this name already exists.'



@pytest.mark.django_db
def test_permissions_view_update_when_data_are_correct_and_user_is_authenticated(
   user, api_client, create_role_data, create_application_data, name_permission
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_application = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data,
    )
    response_create = api_client.post(
        path=reverse("permissions-list"),
        data={
            "role_id": response_role.data["id"],
            "application_id": response_application.data["id"],
            "name": name_permission
        }
    )
    response_update = api_client.put(
        path=reverse("permissions-detail", args=[response_create.data["id"]]),
        data={
            "name": "update_name"
        }
    )
    assert response_update.status_code == 200
    assert response_update.data == {
        "id": response_create.data["id"],
        "role_id": response_role.data["id"],
        "application_id": response_application.data["id"],
        "name": "update_name"
    }


@pytest.mark.django_db
def test_permissions_view_update_when_data_are_correct_and_user_is_not_authenticated(
   user, api_client, create_role_data, create_application_data, name_permission
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_application = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data,
    )
    response_create = api_client.post(
        path=reverse("permissions-list"),
        data={
            "role_id": response_role.data["id"],
            "application_id": response_application.data["id"],
            "name": name_permission
        }
    )
    logout_user(client=api_client)
    response_update = api_client.put(
        path=reverse("permissions-detail", args=[response_create.data["id"]]),
        data={
            "name": "update_name"
        }
    )
    assert response_update.status_code == 401
    assert response_update.data["detail"] == "Authentication credentials were not provided."

@pytest.mark.django_db
def test_permissions_view_delete_and_roles_when_data_are_correct_and_user_is_authenticated(
   user, api_client, create_role_data, create_application_data, name_permission
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_application = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data,
    )
    response_create = api_client.post(
        path=reverse("permissions-list"),
        data={
            "role_id": response_role.data["id"],
            "application_id": response_application.data["id"],
            "name": name_permission
        }
    )
    response_delete = api_client.delete(
        path=reverse("permissions-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 204
    assert response_delete.data is None


@pytest.mark.django_db
def test_permissions_view_create_and_roles_when_data_are_correct_and_user_is_not_authenticated(
   user, api_client, create_role_data, create_application_data, name_permission
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_application = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data,
    )
    response_create = api_client.post(
        path=reverse("permissions-list"),
        data={
            "role_id": response_role.data["id"],
            "application_id": response_application.data["id"],
            "name": name_permission
        }
    )
    logout_user(client=api_client)
    response_delete = api_client.delete(
        path=reverse("permissions-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 401
    assert response_delete.data["detail"] == "Authentication credentials were not provided."
