import pytest

from rest_framework.reverse import reverse

from ..helpful_functions import login_user, logout_user


@pytest.mark.django_db
def test_toxic_roles_view_create_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response = api_client.post(
        path=reverse("toxic_roles-list"),
        data={
            "main_role": response_role.data["id"],
            "toxic_role_id": response_role.data["id"]
        }
    )
    assert response.status_code == 201
    assert response.data == {
        "main_role": response_role.data["id"],
        "toxic_role_id": response_role.data["id"],
        "id": 1
    }


@pytest.mark.django_db
def test_toxic_roles_view_create_when_data_are_correct_and_user_is_not_authenticated(
    user, api_client, create_role_data
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    logout_user(client=api_client)
    response = api_client.post(
        path=reverse("toxic_roles-list"),
        data={
            "main_role": response_role.data["id"],
            "toxic_role_id": response_role.data["id"]
        }
    )
    assert response.status_code == 401
    assert response.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_toxic_roles_view_create_when_main_role_and_toxic_role_id_exsits_in_database(
    api_client, user, create_role_data
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    for _ in range(2):
        response = api_client.post(
            path=reverse("toxic_roles-list"),
            data={
                "main_role": response_role.data["id"],
                "toxic_role_id": response_role.data["id"]
            }
        )
    assert response.status_code == 400
    assert response.data['non_field_errors'][0] == 'The fields main_role, toxic_role_id must make a unique set.'


@pytest.mark.django_db
def test_toxic_roles_view_update_role_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data
):
    login_user(client=api_client, user=user)
    first_response_create_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    create_role_data.update(name="second_test_role")
    second_response_create_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_create_toxic_role = api_client.post(
        path=reverse("toxic_roles-list"),
        data={
            "main_role": first_response_create_role.data["id"],
            "toxic_role_id": second_response_create_role.data["id"]
        }
    )
    response_update_toxic_role = api_client.put(
        path=reverse("toxic_roles-detail", args=[response_create_toxic_role.data["id"]]),
        data={
            "main_role": second_response_create_role.data["id"],
            "toxic_role_id": first_response_create_role.data["id"]
        }
    )
    assert response_update_toxic_role.status_code == 200
    assert response_update_toxic_role.data == {
        "id": response_update_toxic_role.data["id"],
        "main_role": second_response_create_role.data["id"],
        "toxic_role_id": first_response_create_role.data["id"]
    }


@pytest.mark.django_db
def test_toxic_roles_view_update_role_when_data_are_correct_and_user_is_not_authenticated(
    api_client, user, create_role_data
):
    login_user(client=api_client, user=user)
    first_response_create_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    create_role_data.update(name="second_test_role")
    second_response_create_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_create_toxic_role = api_client.post(
        path=reverse("toxic_roles-list"),
        data={
            "main_role": first_response_create_role.data["id"],
            "toxic_role_id": second_response_create_role.data["id"]
        }
    )
    logout_user(client=api_client)
    response_update_toxic_role = api_client.put(
        path=reverse("toxic_roles-detail", args=[response_create_toxic_role.data["id"]]),
        data={
            "main_role": second_response_create_role.data["id"],
            "toxic_role_id": first_response_create_role.data["id"]
        }
    )
    assert response_update_toxic_role.status_code == 401
    assert response_update_toxic_role.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_toxic_roles_view_update_role_when_toxic_role_are_duplicated_and_user_is_authenticated(
    api_client, user, create_role_data
):
    login_user(client=api_client, user=user)
    first_response_create_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    create_role_data.update(name="second_test_role")
    second_response_create_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_create_toxic_role = api_client.post(
        path=reverse("toxic_roles-list"),
        data={
            "main_role": first_response_create_role.data["id"],
            "toxic_role_id": second_response_create_role.data["id"]
        }
    )
    api_client.post(
        path=reverse("toxic_roles-list"),
        data={
            "main_role": second_response_create_role.data["id"],
            "toxic_role_id": second_response_create_role.data["id"]
        }
    )
    response_update_toxic_role = api_client.put(
        path=reverse("toxic_roles-detail", args=[response_create_toxic_role.data["id"]]),
        data={
            "main_role": second_response_create_role.data["id"],
            "toxic_role_id": second_response_create_role.data["id"]
        }
    )
    assert response_update_toxic_role.status_code == 400
    assert response_update_toxic_role.data['non_field_errors'][0] == 'The fields main_role, toxic_role_id must make a unique set.'


@pytest.mark.django_db
def test_toxic_roles_view_delete_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_create = api_client.post(
        path=reverse("toxic_roles-list"),
        data={
            "main_role": response_role.data["id"],
            "toxic_role_id": response_role.data["id"]
        }
    )
    response_delete = api_client.delete(
        path=reverse("toxic_roles-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 204
    assert response_delete.data is None


@pytest.mark.django_db
def test_toxic_roles_view_delete_when_data_are_correct_and_user_is_not_authenticated(
    api_client, user, create_role_data
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_create = api_client.post(
        path=reverse("toxic_roles-list"),
        data={
            "main_role": response_role.data["id"],
            "toxic_role_id": response_role.data["id"]
        }
    )
    logout_user(client=api_client)
    response_delete = api_client.delete(
        path=reverse("toxic_roles-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 401
    assert response_delete.data["detail"] == "Authentication credentials were not provided."
