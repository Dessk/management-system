import pytest

from rest_framework.reverse import reverse

from ..helpful_functions import login_user, logout_user


@pytest.mark.django_db
def test_roles_departaments_view_create_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data, name_departament
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_departament = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    response = api_client.post(
        path=reverse("roles_departaments-list"),
        data={
            "role_id": response_role.data["id"],
            "department_id": response_departament.data["id"],
        }
    )
    assert response.status_code == 201
    assert response.data == {
        "role_id": response_role.data["id"],
        "department_id": response_departament.data["id"],
        "id": 1
    }


@pytest.mark.django_db
def test_roles_departaments_view_create_when_data_are_correct_and_user_is_not_authenticated(
    user, api_client, create_role_data, name_departament
):

    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_departament = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    logout_user(client=api_client)
    response = api_client.post(
        path=reverse("roles_departaments-list"),
        data={
            "role_id": response_role.data["id"],
            "department_id": response_departament.data["id"],
        }
    )
    assert response.status_code == 401
    assert response.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_roles_departaments_view_create_when_role_id_and_department_id_is_not_unique_in_database(
    user, api_client, create_role_data, name_departament
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_departament = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    for _ in range(2):
        response = api_client.post(
            path=reverse("roles_departaments-list"),
            data={
                "role_id": response_role.data["id"],
                "department_id": response_departament.data["id"],
            }
        )
    assert response.status_code == 400
    assert response.data['non_field_errors'][0] == "The fields department_id, role_id must make a unique set."


@pytest.mark.django_db
def test_roles_departaments_view_update_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data, name_departament
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_departament = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    response_departament2 = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": "second_name",
            "chief_id": user.id
        }
    )
    response_create = api_client.post(
        path=reverse("roles_departaments-list"),
        data={
            "role_id": response_role.data["id"],
            "department_id": response_departament.data["id"],
        }
    )
    response_update = api_client.put(
        path=reverse("roles_departaments-detail", args=[response_create.data["id"]]),
        data={
            "role_id": response_role.data["id"],
            "department_id": response_departament2.data["id"],
        }
    )
    assert response_update.status_code == 200
    assert response_update.data == {
        "id": response_create.data["id"],
        "role_id": response_role.data["id"],
        "department_id": response_departament2.data["id"],
    }


@pytest.mark.django_db
def test_roles_departaments_view_update_when_data_are_correct_and_user_is_not_authenticated(
    user, api_client, create_role_data, name_departament
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_departament = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    response_departament2 = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": "second_name",
            "chief_id": user.id
        }
    )
    response_create = api_client.post(
        path=reverse("roles_departaments-list"),
        data={
            "role_id": response_role.data["id"],
            "department_id": response_departament.data["id"],
        }
    )
    logout_user(client=api_client)
    response_update = api_client.put(
        path=reverse("roles_departaments-detail", args=[response_create.data["id"]]),
        data={
            "role_id": response_role.data["id"],
            "department_id": response_departament2.data["id"],
        }
    )
    assert response_update.status_code == 401
    assert response_update.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_roles_departaments_view_delete_and_roles_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data, name_departament
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_departament = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    response_create = api_client.post(
        path=reverse("roles_departaments-list"),
        data={
            "role_id": response_role.data["id"],
            "department_id": response_departament.data["id"],
        }
    )
    response_delete = api_client.delete(
        path=reverse("roles_departaments-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 204
    assert response_delete.data is None


@pytest.mark.django_db
def test_roles_departaments_view_create_and_roles_when_data_are_correct_and_user_is_not_authenticated(
   user, api_client, create_role_data, name_departament
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_departament = api_client.post(
        path=reverse("departaments-list"),
        data={
            "name": name_departament,
            "chief_id": user.id
        }
    )
    response_create = api_client.post(
        path=reverse("roles_departaments-list"),
        data={
            "role_id": response_role.data["id"],
            "department_id": response_departament.data["id"],
        }
    )
    logout_user(client=api_client)
    response_delete = api_client.delete(
        path=reverse("roles_departaments-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 401
    assert response_delete.data["detail"] == "Authentication credentials were not provided."
