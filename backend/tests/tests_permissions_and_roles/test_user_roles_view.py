import pytest

from rest_framework.reverse import reverse

from ..helpful_functions import login_user, logout_user


@pytest.mark.django_db
def test_user_roles_view_create_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response = api_client.post(
        path=reverse("user_roles-list"),
        data={
            "role_id": response_role.data["id"],
            "user_id": user.id
        }
    )
    assert response.status_code == 201
    assert response.data == {
        "role_id": response_role.data["id"],
        "user_id": user.id,
        "id": 1
    }


@pytest.mark.django_db
def test_user_roles_view_create_when_data_are_correct_and_user_is_not_authenticated(
    user, api_client, create_role_data):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    logout_user(client=api_client)
    response = api_client.post(
        path=reverse("user_roles-list"),
        data={
            "role_id": response_role.data["id"],
            "user_id": user.id
        }
    )
    assert response.status_code == 401
    assert response.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_user_roles_view_create_when_user_with_role_exsits_in_database(
    api_client, user, create_role_data
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    for _ in range(2):
        response = api_client.post(
            path=reverse("user_roles-list"),
            data={
                "role_id": response_role.data["id"],
                "user_id": user.id
            }
        )
    assert response.status_code == 400
    assert response.data['non_field_errors'][0] == 'The fields role_id, user_id must make a unique set.'


@pytest.mark.django_db
def test_user_roles_view_update_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data
):
    login_user(client=api_client, user=user)
    first_response_create_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    create_role_data.update(name="second_test_role")
    second_response_create_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_create = api_client.post(
        path=reverse("user_roles-list"),
        data={
            "role_id": first_response_create_role.data["id"],
            "user_id": user.id
        }
    )
    response_update = api_client.put(
        path=reverse("user_roles-detail", args=[response_create.data["id"]]),
        data={
            "role_id": second_response_create_role.data["id"],
            "user_id": user.id
        }
    )
    assert response_update.status_code == 200
    assert response_update.data == {
        "id": response_create.data["id"],
        "role_id": second_response_create_role.data["id"],
        "user_id": user.id
    }


@pytest.mark.django_db
def test_roles_view_update_when_data_are_correct_and_user_is_not_authenticated(
    api_client, user, create_role_data
):
    login_user(client=api_client, user=user)
    first_response_create_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    create_role_data.update(name="second_test_role")
    second_response_create_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_create = api_client.post(
        path=reverse("user_roles-list"),
        data={
            "role_id": first_response_create_role.data["id"],
            "user_id": user.id
        }
    )
    logout_user(client=api_client)
    response_update = api_client.put(
        path=reverse("user_roles-detail", args=[response_create.data["id"]]),
        data={
            "role_id": second_response_create_role.data["id"],
            "user_id": user.id
        }
    )
    assert response_update.status_code == 401
    assert response_update.data["detail"] == "Authentication credentials were not provided."

@pytest.mark.django_db
def test_user_roles_view_delete_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_role_data
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_create = api_client.post(
        path=reverse("user_roles-list"),
        data={
            "role_id": response_role.data["id"],
            "user_id": user.id
        }
    )
    response_delete = api_client.delete(
        path=reverse("user_roles-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 204
    assert response_delete.data is None


@pytest.mark.django_db
def test_user_roles_view_delete_when_data_are_correct_and_user_is_not_authenticated(
    api_client, user, create_role_data
):
    login_user(client=api_client, user=user)
    response_role = api_client.post(
        path=reverse("roles-list"),
        data=create_role_data
    )
    response_create = api_client.post(
        path=reverse("user_roles-list"),
        data={
            "role_id": response_role.data["id"],
            "user_id": user.id
        }
    )
    logout_user(client=api_client)
    response_delete = api_client.delete(
        path=reverse("user_roles-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 401
    assert response_delete.data["detail"] == "Authentication credentials were not provided."
