import pytest

@pytest.fixture
def create_role_data():
    return {
        "name": "test_create_role"
    }


@pytest.fixture
def update_role_data():
    return {
        "name": "test_update_role"
    }


@pytest.fixture
def name_departament():
    return "test_departament"


@pytest.fixture
def create_application_data():
    return {
        "name": "create_name",
        "type": "create_type",
        "description": "create_desc",
        "classification": 1,
        "technology": "create_technology"
    }


@pytest.fixture
def update_application_data():
    return {
        "name": "update_name",
        "type": "update_type",
        "description": "update_desc",
        "classification": 2,
        "technology": "update_technology"
    }


@pytest.fixture
def name_permission():
    return "test_permission"