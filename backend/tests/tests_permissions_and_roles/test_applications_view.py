import pytest

from rest_framework.reverse import reverse

from ..helpful_functions import login_user, logout_user


@pytest.mark.django_db
def test_applications_view_create_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_application_data
):
    login_user(client=api_client, user=user)
    response = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data
    )
    assert response.status_code == 201
    assert response.data == {
        "id": 1,
        "name": "create_name",
        "type": "create_type",
        "description": "create_desc",
        "classification": 1,
        "technology": "create_technology"
    }


@pytest.mark.django_db
def test_applications_view_create_when_data_are_correct_and_user_is_not_authenticated(api_client, create_application_data):
    response = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data
    )
    assert response.status_code == 401
    assert response.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_applications_view_create_when_name_exsits_in_database(
    api_client, user, create_application_data
):
    login_user(client=api_client, user=user)
    for _ in range(2):
        response = api_client.post(
            path=reverse("applications-list"),
            data=create_application_data
        )
    assert response.status_code == 400
    assert response.data["name"][0] == 'applications with this name already exists.'


@pytest.mark.django_db
def test_applications_view_update_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_application_data, update_application_data
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data
    )
    response_update = api_client.put(
        path=reverse("applications-detail", args=[response_create.data["id"]]),
        data=update_application_data
    )
    assert response_update.status_code == 200
    assert response_update.data == {
        "id": response_create.data["id"],
        "name": "update_name",
        "type": "update_type",
        "description": "update_desc",
        "classification": 2,
        "technology": "update_technology"
    }


@pytest.mark.django_db
def test_applications_view_update_when_data_are_correct_and_user_is_not_authenticated(
    api_client, user, create_application_data, update_application_data
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data
    )
    logout_user(client=api_client)
    response_update = api_client.put(
        path=reverse("applications-detail", args=[response_create.data["id"]]),
        data=update_application_data
    )
    assert response_update.status_code == 401
    assert response_update.data["detail"] == "Authentication credentials were not provided."


@pytest.mark.django_db
def test_applications_view_delete_when_data_are_correct_and_user_is_authenticated(
    user, api_client, create_application_data
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data
    )
    response_delete = api_client.delete(
        path=reverse("applications-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 204
    assert response_delete.data is None


@pytest.mark.django_db
def test_applications_view_delete_when_data_are_correct_and_user_is_not_authenticated(
    api_client, user, create_application_data
):
    login_user(client=api_client, user=user)
    response_create = api_client.post(
        path=reverse("applications-list"),
        data=create_application_data
    )
    logout_user(client=api_client)
    response_delete = api_client.delete(
        path=reverse("applications-detail", args=[response_create.data["id"]]),
        data={"id": response_create.data["id"]}
    )
    assert response_delete.status_code == 401
    assert response_delete.data["detail"] == "Authentication credentials were not provided."
