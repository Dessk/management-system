from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import (
    ApplicationsViewSet,
    DepartamentsViewSet,
    PermissionsViewSet,
    RolesDepartamentsViewSet,
    RolesViewSet,
    ToxicRolesViewSet,
    UserRolesViewSet
)


ROUTER = DefaultRouter()
ROUTER.register(r"roles", RolesViewSet, "roles")
ROUTER.register(r"toxic_roles", ToxicRolesViewSet, "toxic_roles")
ROUTER.register(r"user_roles", UserRolesViewSet, "user_roles")
ROUTER.register(r"departaments", DepartamentsViewSet, "departaments")
ROUTER.register(r"roles_departaments", RolesDepartamentsViewSet, "roles_departaments")
ROUTER.register(r"applications", ApplicationsViewSet, "applications")
ROUTER.register(r"permissions", PermissionsViewSet, "permissions")


urlpatterns = [
    path("", include(ROUTER.urls)),
]
