from rest_framework import serializers

from .models import (
    Applications,
    Departaments,
    Permissions,
    Roles,
    RolesDepartaments,
    ToxicRoles,
    UserRoles
)


class RolesSerializer(serializers.ModelSerializer):
    """
    Serializer for roles.
    """
    class Meta:
        model = Roles
        fields = [
            "name",
            "id"
        ]
        read_only_fields = ["id"]


class ToxicRolesSerializer(serializers.ModelSerializer):
    """
    Serializer for toxic roles.
    """
    class Meta:
        model = ToxicRoles
        fields = [
            "main_role",
            "toxic_role_id",
            "id"
        ]
        read_only_fields = ["id"]


class UserRolesSerializer(serializers.ModelSerializer):
    """
    Serializer for user roles.
    """
    class Meta:
        model = UserRoles
        fields = [
            "id",
            "role_id",
            "user_id"
        ]
        read_only_fields = ["id"]


class DepartamentsSerializer(serializers.ModelSerializer):
    """
    Serializer for departaments.
    """
    class Meta:
        model = Departaments
        fields = [
            "id",
            "chief_id",
            "name"
        ]
        read_only_fields = ["id"]


class RolesDepartamentsSerializer(serializers.ModelSerializer):
    """
    Serializer for roles departaments.
    """
    class Meta:
        model = RolesDepartaments
        fields = [
            "id",
            "department_id",
            "role_id"
        ]
        read_only_fields = ["id"]


class ApplicationsSerializer(serializers.ModelSerializer):
    """
    Serializer for applications serializer.
    """
    class Meta:
        model = Applications
        fields = [
            "id",
            "name",
            "type",
            "description",
            "classification",
            "technology"
        ]
        read_only_fields = ["id"]


class PermissionsSerializer(serializers.ModelSerializer):
    """
    Serializer for permissions serializer.
    """
    class Meta:
        model = Permissions
        fields = [
            "id",
            "name",
            "role_id",
            "application_id"
        ]
        read_only_fields = ["id"]
        extra_kwargs = {
            "role_id": {"required": False},
            "application_id": {"required": False},
        }
        