from django.db import models
from django.utils.translation import gettext_lazy as _

from authorization.models import User


class Roles(models.Model):
    """
    Roles model.
    """
    name = models.CharField(
        verbose_name=_("name"),
        max_length=255,
        unique=True
    )

    def __str__(self):
        return self.name


class ToxicRoles(models.Model):
    """
    Toxic Roles model.
    """
    main_role = models.ForeignKey(
        to=Roles,
        on_delete=models.CASCADE,
        related_name="main_role"
    )
    toxic_role_id = models.ForeignKey(
        to=Roles,
        on_delete=models.CASCADE,
        related_name="toxic_role"
    )
    class Meta:
        unique_together = [("main_role", "toxic_role_id")]


class UserRoles(models.Model):
    """
    User Roles model.
    """
    role_id = models.ForeignKey(
        to=Roles,
        on_delete=models.CASCADE
    )
    user_id = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE
    )
    class Meta:
        unique_together = [("role_id", "user_id")]


class Departaments(models.Model):
    """
    Departaments model.
    """
    chief_id = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE
    )
    name = models.CharField(
        verbose_name=_("name"),
        max_length=1000,
        unique=True
    )
    class Meta:
        unique_together = [("name", "chief_id")]

    def __str__(self):
        return self.name


class RolesDepartaments(models.Model):
    """
    Roles Departaments model.
    """
    department_id = models.ForeignKey(
        to=Departaments,
        on_delete=models.CASCADE
    )
    role_id = models.ForeignKey(
        to=Roles,
        on_delete=models.CASCADE
    )
    class Meta:
        unique_together = [("department_id", "role_id")]


class Applications(models.Model):
    """
    Applications model.
    """
    name = models.CharField(
        verbose_name=_("name"),
        max_length=255,
        unique=True
    )
    type = models.CharField(
        verbose_name=_("type"),
        max_length=50
    )
    description = models.CharField(
        verbose_name=_("description"),
        max_length=1000
    )
    classification = models.IntegerField(
        verbose_name=_("classification")
    )
    technology = models.CharField(
        verbose_name=_("technology"),
        max_length=255
    )

    def __str__(self):
        return self.name


class Permissions(models.Model):
    """
    Permissions model.
    """
    name = models.CharField(
        verbose_name=_("name"),
        max_length=255,
        unique=True
    )
    role_id = models.ForeignKey(
        to=Roles,
        on_delete=models.CASCADE
    )
    application_id = models.ForeignKey(
        to=Applications,
        on_delete=models.CASCADE
    )
