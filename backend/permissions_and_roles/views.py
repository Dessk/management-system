from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet


from .models import (
    Applications,
    Departaments,
    Permissions,
    Roles,
    RolesDepartaments,
    ToxicRoles,
    UserRoles
)
from .serializers import (
    ApplicationsSerializer,
    DepartamentsSerializer,
    PermissionsSerializer,
    RolesDepartamentsSerializer,
    RolesSerializer,
    ToxicRolesSerializer,
    UserRolesSerializer
)


class RolesViewSet(ModelViewSet):
    """
    Roles end point.
    """
    serializer_class = RolesSerializer
    queryset = Roles.objects.all()
    permission_classes = [IsAuthenticated]


class ToxicRolesViewSet(ModelViewSet):
    """
    Toxic Roles end point.
    """
    serializer_class = ToxicRolesSerializer
    queryset = ToxicRoles.objects.all()
    permission_classes = [IsAuthenticated]


class UserRolesViewSet(ModelViewSet):
    """
    User Roles end point.
    """
    serializer_class = UserRolesSerializer
    queryset = UserRoles.objects.all()
    permission_classes = [IsAuthenticated]


class DepartamentsViewSet(ModelViewSet):
    """
    Departaments end point.
    """
    serializer_class = DepartamentsSerializer
    queryset = Departaments.objects.all()
    permission_classes = [IsAuthenticated]


class RolesDepartamentsViewSet(ModelViewSet):
    """
    Roles Departaments end point.
    """
    serializer_class = RolesDepartamentsSerializer
    queryset = RolesDepartaments.objects.all()
    permission_classes = [IsAuthenticated]


class ApplicationsViewSet(ModelViewSet):
    """
    Applications end point.
    """
    serializer_class = ApplicationsSerializer
    queryset = Applications.objects.all()
    permission_classes = [IsAuthenticated]


class PermissionsViewSet(ModelViewSet):
    """
    Pernissions end point.
    """
    serializer_class = PermissionsSerializer
    queryset = Permissions.objects.all()
    permission_classes = [IsAuthenticated]
