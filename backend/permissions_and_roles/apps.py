from django.apps import AppConfig


class PermissionsAndRolesConfig(AppConfig):
    "Config permissions and roles applications."
    name = 'permissions_and_roles'
