authorization.migrations package
================================

Submodules
----------

authorization.migrations.0001\_initial module
---------------------------------------------

.. automodule:: authorization.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: authorization.migrations
   :members:
   :undoc-members:
   :show-inheritance:
