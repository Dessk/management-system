permissions\_and\_roles.migrations package
==========================================

Submodules
----------

permissions\_and\_roles.migrations.0001\_initial module
-------------------------------------------------------

.. automodule:: permissions_and_roles.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

permissions\_and\_roles.migrations.0002\_auto\_20200510\_1413 module
--------------------------------------------------------------------

.. automodule:: permissions_and_roles.migrations.0002_auto_20200510_1413
   :members:
   :undoc-members:
   :show-inheritance:

permissions\_and\_roles.migrations.0003\_auto\_20200510\_1541 module
--------------------------------------------------------------------

.. automodule:: permissions_and_roles.migrations.0003_auto_20200510_1541
   :members:
   :undoc-members:
   :show-inheritance:

permissions\_and\_roles.migrations.0004\_auto\_20200510\_1711 module
--------------------------------------------------------------------

.. automodule:: permissions_and_roles.migrations.0004_auto_20200510_1711
   :members:
   :undoc-members:
   :show-inheritance:

permissions\_and\_roles.migrations.0005\_auto\_20200510\_1822 module
--------------------------------------------------------------------

.. automodule:: permissions_and_roles.migrations.0005_auto_20200510_1822
   :members:
   :undoc-members:
   :show-inheritance:

permissions\_and\_roles.migrations.0006\_auto\_20200513\_2045 module
--------------------------------------------------------------------

.. automodule:: permissions_and_roles.migrations.0006_auto_20200513_2045
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: permissions_and_roles.migrations
   :members:
   :undoc-members:
   :show-inheritance:
