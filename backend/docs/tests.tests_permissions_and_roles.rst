tests.tests\_permissions\_and\_roles package
============================================

Submodules
----------

tests.tests\_permissions\_and\_roles.conftest module
----------------------------------------------------

.. automodule:: tests.tests_permissions_and_roles.conftest
   :members:
   :undoc-members:
   :show-inheritance:

tests.tests\_permissions\_and\_roles.test\_applications\_view module
--------------------------------------------------------------------

.. automodule:: tests.tests_permissions_and_roles.test_applications_view
   :members:
   :undoc-members:
   :show-inheritance:

tests.tests\_permissions\_and\_roles.test\_departaments\_view module
--------------------------------------------------------------------

.. automodule:: tests.tests_permissions_and_roles.test_departaments_view
   :members:
   :undoc-members:
   :show-inheritance:

tests.tests\_permissions\_and\_roles.test\_perrmissions\_view module
--------------------------------------------------------------------

.. automodule:: tests.tests_permissions_and_roles.test_perrmissions_view
   :members:
   :undoc-members:
   :show-inheritance:

tests.tests\_permissions\_and\_roles.test\_roles\_departaments\_view module
---------------------------------------------------------------------------

.. automodule:: tests.tests_permissions_and_roles.test_roles_departaments_view
   :members:
   :undoc-members:
   :show-inheritance:

tests.tests\_permissions\_and\_roles.test\_roles\_view module
-------------------------------------------------------------

.. automodule:: tests.tests_permissions_and_roles.test_roles_view
   :members:
   :undoc-members:
   :show-inheritance:

tests.tests\_permissions\_and\_roles.test\_toxic\_roles\_view module
--------------------------------------------------------------------

.. automodule:: tests.tests_permissions_and_roles.test_toxic_roles_view
   :members:
   :undoc-members:
   :show-inheritance:

tests.tests\_permissions\_and\_roles.test\_user\_roles\_view module
-------------------------------------------------------------------

.. automodule:: tests.tests_permissions_and_roles.test_user_roles_view
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: tests.tests_permissions_and_roles
   :members:
   :undoc-members:
   :show-inheritance:
