permissions\_and\_roles package
===============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   permissions_and_roles.migrations

Submodules
----------

permissions\_and\_roles.apps module
-----------------------------------

.. automodule:: permissions_and_roles.apps
   :members:
   :undoc-members:
   :show-inheritance:

permissions\_and\_roles.models module
-------------------------------------

.. automodule:: permissions_and_roles.models
   :members:
   :undoc-members:
   :show-inheritance:

permissions\_and\_roles.serializers module
------------------------------------------

.. automodule:: permissions_and_roles.serializers
   :members:
   :undoc-members:
   :show-inheritance:

permissions\_and\_roles.urls module
-----------------------------------

.. automodule:: permissions_and_roles.urls
   :members:
   :undoc-members:
   :show-inheritance:

permissions\_and\_roles.views module
------------------------------------

.. automodule:: permissions_and_roles.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: permissions_and_roles
   :members:
   :undoc-members:
   :show-inheritance:
