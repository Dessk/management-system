tests package
=============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   tests.tests_authorization
   tests.tests_permissions_and_roles

Submodules
----------

tests.conftest module
---------------------

.. automodule:: tests.conftest
   :members:
   :undoc-members:
   :show-inheritance:

tests.helpful\_functions module
-------------------------------

.. automodule:: tests.helpful_functions
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: tests
   :members:
   :undoc-members:
   :show-inheritance:
