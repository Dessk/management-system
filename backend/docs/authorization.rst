authorization package
=====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   authorization.migrations

Submodules
----------

authorization.apps module
-------------------------

.. automodule:: authorization.apps
   :members:
   :undoc-members:
   :show-inheritance:

authorization.authentication module
-----------------------------------

.. automodule:: authorization.authentication
   :members:
   :undoc-members:
   :show-inheritance:

authorization.models module
---------------------------

.. automodule:: authorization.models
   :members:
   :undoc-members:
   :show-inheritance:

authorization.serializers module
--------------------------------

.. automodule:: authorization.serializers
   :members:
   :undoc-members:
   :show-inheritance:

authorization.urls module
-------------------------

.. automodule:: authorization.urls
   :members:
   :undoc-members:
   :show-inheritance:

authorization.views module
--------------------------

.. automodule:: authorization.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: authorization
   :members:
   :undoc-members:
   :show-inheritance:
