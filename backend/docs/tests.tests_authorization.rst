tests.tests\_authorization package
==================================

Submodules
----------

tests.tests\_authorization.test\_login\_view module
---------------------------------------------------

.. automodule:: tests.tests_authorization.test_login_view
   :members:
   :undoc-members:
   :show-inheritance:

tests.tests\_authorization.test\_register\_view module
------------------------------------------------------

.. automodule:: tests.tests_authorization.test_register_view
   :members:
   :undoc-members:
   :show-inheritance:

tests.tests\_authorization.test\_reset\_password\_view module
-------------------------------------------------------------

.. automodule:: tests.tests_authorization.test_reset_password_view
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: tests.tests_authorization
   :members:
   :undoc-members:
   :show-inheritance:
