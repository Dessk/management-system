import React from "react";
import { Route, Redirect} from "react-router-dom";
import { useContextAuth } from "./context/AuthContext";


const PrivateRoute = ({ component: Component, ...rest }) => {
  const {token} = useContextAuth();
  
  return (
    <Route
      {...rest}
      render={props => {
        if (token) {
          return  <Component {...props} />;
        }
        return <Redirect to={{ pathname: "/login", state: { referer: props.location } }}/>;
      }
      }
    />
  );
}

export default PrivateRoute;