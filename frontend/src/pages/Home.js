import React from "react";
import {Link} from "react-router-dom"
import { useContextAuth } from "../context/AuthContext";

const Home = () => {
    const {username} = useContextAuth();
    return (
        <ul>
          <li>
            <Link to="/">Home Page</Link>
          </li>
          <li>
            <Link to="/registration">Registration</Link>
          </li>
          <li>
            <Link to="/login">Login</Link>
          </li>
          {username &&
        <li>
          <Link to="/test">Test</Link>
        </li>
        
      }
      </ul>
    );
};

export default Home;