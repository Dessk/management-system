import React, {useState} from "react";
import axios from "axios";
import { Link, Redirect } from 'react-router-dom';
import {Formik, Form, ErrorMessage, Field} from "formik"
import * as Yup from 'yup';
import {useContextAuth} from '../context/AuthContext'
import {errorMessages} from "./errorMessages"
import '../css/styles.css';


const Login = () => {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [responseError, setResponseError] = useState(null);
  const [requestError, setRequestError] = useState(null);
  const {setToken, setUsername, setUserId} = useContextAuth();

  if (isLoggedIn) {
    return <Redirect to='/'/>;
  }

  return(
    <Formik
      initialValues={{
        pesel: '',
        password: ''
      }}
      validationSchema={Yup.object({
        pesel: Yup.string().required(errorMessages["required"]),
        password: Yup.string().required(errorMessages["required"])
      })}
      onSubmit={async (values) => {
        try {
          const response = await axios.post(
            "http://127.0.0.1:8000/api-auth/login/",
            values,
            {
              headers: {
                "Content-Type": "application/json"
            }}
          );
          const {token, username, id} = response.data;
          setToken(token);
          setUserId(id);
          setUsername(username);
          setLoggedIn(true);
        } catch (error) {
          if (error.response) {
            setResponseError(error.response.data)
          }
          else if (error.request) {
            setRequestError(error.request)
          }
        }
      }}
    >
    <Form>
      <label htmlFor="pesel">Pesel</label>
      <Field name="pesel" type="text" />
      <div className="error">
        <ErrorMessage name="pesel" />
      </div>
      <label htmlFor="password">Password</label>
      <Field name="password" type="password"/>
      <div className="error">
        <ErrorMessage name="password"/>
      </div>
      <div className="error">
        {responseError ? responseError.nonFieldErrors: null}
      </div>
      <div className="error">
        {requestError ? requestError: null}
      </div>
      <button type="submit">Login</button>
      <br/>
      <div>
        <Link to="/registration">Don't have an account?</Link>
      </div>
      <div>
        <Link to="/reset_password">Don't remember password?</Link>
      </div>
    </Form>
    </Formik>
);
}

export default Login;