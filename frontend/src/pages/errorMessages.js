export const errorMessages = {
    "required": "This field is required.",
    "max": "The value in this field must be less than or equal ",
    "min": "The value in this field must be greater than or equal ",
    "only_letters": "This field can contain only letters."
  }
