import React, {useState, useEffect} from 'react';
import Recaptcha from "react-recaptcha";
import axios from "axios";
import {Redirect} from 'react-router-dom';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {errorMessages} from "./errorMessages"
import '../css/styles.css';


const Registration = () => {
  const country = [['AW', 'Aruba'], ['AF', 'Afghanistan'], ['AO', 'Angola'], ['AI', 'Anguilla'], ['AX', 'Åland Islands'], ['AL', 'Albania'], ['AD', 'Andorra'], ['AE', 'United Arab Emirates'], ['AR', 'Argentina'], ['AM', 'Armenia'], ['AS', 'American Samoa'], ['AQ', 'Antarctica'], ['TF', 'French Southern Territories'], ['AG', 'Antigua and Barbuda'], ['AU', 'Australia'], ['AT', 'Austria'], ['AZ', 'Azerbaijan'], ['BI', 'Burundi'], ['BE', 'Belgium'], ['BJ', 'Benin'], ['BQ', 'Bonaire, Sint Eustatius and Saba'], ['BF', 'Burkina Faso'], ['BD', 'Bangladesh'], ['BG', 'Bulgaria'], ['BH', 'Bahrain'], ['BS', 'Bahamas'], ['BA', 'Bosnia and Herzegovina'], ['BL', 'Saint Barthélemy'], ['BY', 'Belarus'], ['BZ', 'Belize'], ['BM', 'Bermuda'], ['BO', 'Bolivia, Plurinational State of'], ['BR', 'Brazil'], ['BB', 'Barbados'], ['BN', 'Brunei Darussalam'], ['BT', 'Bhutan'], ['BV', 'Bouvet Island'], ['BW', 'Botswana'], ['CF', 'Central African Republic'], ['CA', 'Canada'], ['CC', 'Cocos (Keeling) Islands'], ['CH', 'Switzerland'], ['CL', 'Chile'], ['CN', 'China'], ['CI', "Côte d'Ivoire"], ['CM', 'Cameroon'], ['CD', 'Congo, The Democratic Republic of the'], ['CG', 'Congo'], ['CK', 'Cook Islands'], ['CO', 'Colombia'], ['KM', 'Comoros'], ['CV', 'Cabo Verde'], ['CR', 'Costa Rica'], ['CU', 'Cuba'], ['CW', 'Curaçao'], ['CX', 'Christmas Island'], ['KY', 'Cayman Islands'], ['CY', 'Cyprus'], ['CZ', 'Czechia'], ['DE', 'Germany'], ['DJ', 'Djibouti'], ['DM', 'Dominica'], ['DK', 'Denmark'], ['DO', 'Dominican Republic'], ['DZ', 'Algeria'], ['EC', 'Ecuador'], ['EG', 'Egypt'], ['ER', 'Eritrea'], ['EH', 'Western Sahara'], ['ES', 'Spain'], ['EE', 'Estonia'], ['ET', 'Ethiopia'], ['FI', 'Finland'], ['FJ', 'Fiji'], ['FK', 'Falkland Islands (Malvinas)'], ['FR', 'France'], ['FO', 'Faroe Islands'], ['FM', 'Micronesia, Federated States of'], ['GA', 'Gabon'], ['GB', 'United Kingdom'], ['GE', 'Georgia'], ['GG', 'Guernsey'], ['GH', 'Ghana'], ['GI', 'Gibraltar'], ['GN', 'Guinea'], ['GP', 'Guadeloupe'], ['GM', 'Gambia'], ['GW', 'Guinea-Bissau'], ['GQ', 'Equatorial Guinea'], ['GR', 'Greece'], ['GD', 'Grenada'], ['GL', 'Greenland'], ['GT', 'Guatemala'], ['GF', 'French Guiana'], ['GU', 'Guam'], ['GY', 'Guyana'], ['HK', 'Hong Kong'], ['HM', 'Heard Island and McDonald Islands'], ['HN', 'Honduras'], ['HR', 'Croatia'], ['HT', 'Haiti'], ['HU', 'Hungary'], ['ID', 'Indonesia'], ['IM', 'Isle of Man'], ['IN', 'India'], ['IO', 'British Indian Ocean Territory'], ['IE', 'Ireland'], ['IR', 'Iran, Islamic Republic of'], ['IQ', 'Iraq'], ['IS', 'Iceland'], ['IL', 'Israel'], ['IT', 'Italy'], ['JM', 'Jamaica'], ['JE', 'Jersey'], ['JO', 'Jordan'], ['JP', 'Japan'], ['KZ', 'Kazakhstan'], ['KE', 'Kenya'], ['KG', 'Kyrgyzstan'], ['KH', 'Cambodia'], ['KI', 'Kiribati'], ['KN', 'Saint Kitts and Nevis'], ['KR', 'Korea, Republic of'], ['KW', 'Kuwait'], ['LA', "Lao People's Democratic Republic"], ['LB', 'Lebanon'], ['LR', 'Liberia'], ['LY', 'Libya'], ['LC', 'Saint Lucia'], ['LI', 'Liechtenstein'], ['LK', 'Sri Lanka'], ['LS', 'Lesotho'], ['LT', 'Lithuania'], ['LU', 'Luxembourg'], ['LV', 'Latvia'], ['MO', 'Macao'], ['MF', 'Saint Martin (French part)'], ['MA', 'Morocco'], ['MC', 'Monaco'], ['MD', 'Moldova, Republic of'], ['MG', 'Madagascar'], ['MV', 'Maldives'], ['MX', 'Mexico'], ['MH', 'Marshall Islands'], ['MK', 'North Macedonia'], ['ML', 'Mali'], ['MT', 'Malta'], ['MM', 'Myanmar'], ['ME', 'Montenegro'], ['MN', 'Mongolia'], ['MP', 'Northern Mariana Islands'], ['MZ', 'Mozambique'], ['MR', 'Mauritania'], ['MS', 'Montserrat'], ['MQ', 'Martinique'], ['MU', 'Mauritius'], ['MW', 'Malawi'], ['MY', 'Malaysia'], ['YT', 'Mayotte'], ['NA', 'Namibia'], ['NC', 'New Caledonia'], ['NE', 'Niger'], ['NF', 'Norfolk Island'], ['NG', 'Nigeria'], ['NI', 'Nicaragua'], ['NU', 'Niue'], ['NL', 'Netherlands'], ['NO', 'Norway'], ['NP', 'Nepal'], ['NR', 'Nauru'], ['NZ', 'New Zealand'], ['OM', 'Oman'], ['PK', 'Pakistan'], ['PA', 'Panama'], ['PN', 'Pitcairn'], ['PE', 'Peru'], ['PH', 'Philippines'], ['PW', 'Palau'], ['PG', 'Papua New Guinea'], ['PL', 'Poland'], ['PR', 'Puerto Rico'], ['KP', "Korea, Democratic People's Republic of"], ['PT', 'Portugal'], ['PY', 'Paraguay'], ['PS', 'Palestine, State of'], ['PF', 'French Polynesia'], ['QA', 'Qatar'], ['RE', 'Réunion'], ['RO', 'Romania'], ['RU', 'Russian Federation'], ['RW', 'Rwanda'], ['SA', 'Saudi Arabia'], ['SD', 'Sudan'], ['SN', 'Senegal'], ['SG', 'Singapore'], ['GS', 'South Georgia and the South Sandwich Islands'], ['SH', 'Saint Helena, Ascension and Tristan da Cunha'], ['SJ', 'Svalbard and Jan Mayen'], ['SB', 'Solomon Islands'], ['SL', 'Sierra Leone'], ['SV', 'El Salvador'], ['SM', 'San Marino'], ['SO', 'Somalia'], ['PM', 'Saint Pierre and Miquelon'], ['RS', 'Serbia'], ['SS', 'South Sudan'], ['ST', 'Sao Tome and Principe'], ['SR', 'Suriname'], ['SK', 'Slovakia'], ['SI', 'Slovenia'], ['SE', 'Sweden'], ['SZ', 'Eswatini'], ['SX', 'Sint Maarten (Dutch part)'], ['SC', 'Seychelles'], ['SY', 'Syrian Arab Republic'], ['TC', 'Turks and Caicos Islands'], ['TD', 'Chad'], ['TG', 'Togo'], ['TH', 'Thailand'], ['TJ', 'Tajikistan'], ['TK', 'Tokelau'], ['TM', 'Turkmenistan'], ['TL', 'Timor-Leste'], ['TO', 'Tonga'], ['TT', 'Trinidad and Tobago'], ['TN', 'Tunisia'], ['TR', 'Turkey'], ['TV', 'Tuvalu'], ['TW', 'Taiwan, Province of China'], ['TZ', 'Tanzania, United Republic of'], ['UG', 'Uganda'], ['UA', 'Ukraine'], ['UM', 'United States Minor Outlying Islands'], ['UY', 'Uruguay'], ['US', 'United States'], ['UZ', 'Uzbekistan'], ['VA', 'Holy See (Vatican City State)'], ['VC', 'Saint Vincent and the Grenadines'], ['VE', 'Venezuela, Bolivarian Republic of'], ['VG', 'Virgin Islands, British'], ['VI', 'Virgin Islands, U.S.'], ['VN', 'Viet Nam'], ['VU', 'Vanuatu'], ['WF', 'Wallis and Futuna'], ['WS', 'Samoa'], ['YE', 'Yemen'], ['ZA', 'South Africa'], ['ZM', 'Zambia'], ['ZW', 'Zimbabwe']]
  const [recaptcha, setRecaptcha] = useState(null);
  const [isRegister, setRegister] = useState(false);
  const [responseError, setResponseError] = useState(null);
  const [requestError, setRequestError] = useState(null);

  useEffect(() => {
    const script = document.createElement("script");
    script.src = "https://www.google.com/recaptcha/api.js";
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);
  });

  if (isRegister) {
    return <Redirect to='/'/>;
  }

  return (
    <Formik
      initialValues={{
        sex: 'M',
        country: 'AW',
        username: '',
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        passwordConfirm: '',
        pesel: '',
      }}
      validationSchema={Yup.object({
        firstName: Yup.string()
          .max(30, errorMessages["max"] + "30")
          .min(3, errorMessages["min"] + "3")
          .required(errorMessages['required'])
          .matches(/^[\s\p{L}]+$/u, errorMessages["only_letters"]),
        password: Yup.string().required(errorMessages["required"]),
        passwordConfirm: Yup.string().required(errorMessages["required"]),
        lastName: Yup.string()
          .max(30, errorMessages["max"] + "150")
          .min(3, errorMessages["min"] + "3")
          .required(errorMessages['required'])
          .matches(/^[\s\p{L}]+$/u, errorMessages["only_letters"]),
        username: Yup.string()
          .max(30, errorMessages["max"] + "150")
          .min(3, errorMessages["min"] + "3")
          .required(errorMessages['required']),
        email: Yup.string()
          .email("Invalid email address.")
          .required(errorMessages['required']),
        pesel: Yup.string()
          .max(11, errorMessages['max'] + "11")
          .min(3, errorMessages["min"] + "11")
          .required(errorMessages['required']),
      }
      )}
      onSubmit={async (values) => {
        if (!recaptcha) {
          alert("Recaptcha is required.")
          return
        }
        try {
          await axios.post(
            "http://127.0.0.1:8000/api-auth/registration/",
            JSON.stringify(values),
            {
              headers: {
                "Content-Type": "application/json"
              }
            }
          );
          setRegister(true);
        } catch (error) {
          if (error.response) {
            setResponseError(error.response.data)
          }
          else if (error.request) {
            setRequestError(error.request)
          }
        }
      }}
    >
      <Form>
        <label htmlFor="username">Username</label>
        <Field name="username" type="text" />
        <div className="error">
          <ErrorMessage name="username" />
          {responseError ? responseError.username: null}
        </div>
        <label htmlFor="password">Password</label>
        <Field name="password" type="password"/>
        <div className="error">
          <ErrorMessage name="password"/>
          {responseError ? responseError.password: null}
        </div>
        <label htmlFor="passwordConfirm">Password confirm</label>
        <Field name="passwordConfirm" type="password"/>
        <div className="error">
          <ErrorMessage name="passwordConfirm"/>
          {responseError ? responseError.passwordConfirm: null}
          {responseError ? responseError.nonFieldErrors: null}
        </div>
        <label htmlFor="email">Email Address</label>
        <Field name="email" type="email" />
        <div className="error">
          <ErrorMessage name="email" />
          {responseError ? responseError.email: null}
        </div>
        <label htmlFor="firstName">First Name</label>
        <Field name="firstName" type="text" />
        <div className="error">
          <ErrorMessage name="firstName" />
          {responseError ? responseError.firstName: null}
        </div>
        <label htmlFor="lastName">Last Name</label>
        <Field name="lastName" type="text" />
        <div className="error">
          <ErrorMessage name="lastName" />
          {responseError ? responseError.lastName: null}
        </div>
        <label htmlFor="sex">Sex</label>
        <Field id="sex" name="sex" checked value="M" type="radio"/> <span className="sex_text">Male</span>
        <Field id="sex" name="sex" value="F" type="radio"/> <span className="sex_text">Female</span>
        <label htmlFor="country">Country</label>
        <Field name="country" as="select">
            {country.map((value, idx) => <option value={value[0]} key={idx}>{value[1]}</option>)}
        </Field>
        <label htmlFor="pesel">Pesel</label>
        <Field name="pesel" type="text" />
        <div className="error">
            <ErrorMessage name="pesel" />
            {responseError ? responseError.pesel: null}
        </div>
        <br/>
        <Recaptcha
          sitekey="6Le2nREUAAAAALYuOv7X9Fe3ysDmOmghtj0dbCKW"
          render="explicit"
          theme="dark"
          onloadCallback={() => null}
          verifyCallback={(response) => setRecaptcha(response)}
        />
        <div className="error">
          {requestError ? requestError: null}
        </div>
        <button type="submit">Register</button>
      </Form>
    </Formik>
  );
}

export default Registration;