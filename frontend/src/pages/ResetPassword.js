import React, {useState} from "react";
import axios from "axios";
import {Redirect } from 'react-router-dom';
import {Formik, Field, Form, ErrorMessage} from "formik";
import * as Yup from 'yup';
import {errorMessages} from "./errorMessages"


const ResetPassword = () => {
  const [resetPassword, setResetPassword] = useState(false);
  const [responseError, setResponseError] = useState(null);
  const [requestError, setRequestError] = useState(null);

  if (resetPassword) {
    return <Redirect to='/login'/>;
  }

  return(
    <Formik
      initialValues={{
        pesel: '',
        email: ''
      }}
      validationSchema={Yup.object({
        pesel: Yup.string().required(errorMessages["required"]),
        email: Yup.string()
          .email("Invalid email address.")
          .required(errorMessages["required"])
      })}
      onSubmit={async (values) => {
        try {
          await axios.post(
            "http://127.0.0.1:8000/api-auth/reset_password",
            JSON.stringify(values),
            {
              headers: {
                "Content-Type": "application/json"
              }
            }
          );
          setResetPassword(true);
        } catch (error) {
          if (error.response) {
            setResponseError(error.response.data)
          }
          else if (error.request) {
            setRequestError(error.request)
          }
        }
      }}
    >
      <Form>
        <label htmlFor="pesel">Pesel</label>
        <Field name="pesel" type="text" />
        <div className="error">
          <ErrorMessage name="pesel" />
        </div>
        <label htmlFor="email">Email Address</label>
        <Field name="email" type="email" />
        <div className="error">
          <ErrorMessage name="email" />
        </div>
        <div className="error">
          {responseError ? responseError.nonFieldErrors: null}
        </div>
        <br/>
        <div className="error">
          {requestError ? requestError: null}
        </div>
        <button type="submit">Reset</button>
      </Form>
    </Formik>
  );
}

export default ResetPassword;