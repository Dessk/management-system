import React, {useState} from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import Registration from './pages/Registration';
import Login from "./pages/Login";
import Home from "./pages/Home";
import ResetPassword from "./pages/ResetPassword";
import {AuthContext} from './context/AuthContext';
import PrivateRoute from './PrivateRoute';

const Test = () => {
  return <p>token test</p>
}

const App = () => {
  const [token, setToken] = useState(null);
  const [username, setUsername] = useState(null);
  const [userId, setUserId] = useState(null);

  return (
    <AuthContext.Provider value={{
      token: token,
      setToken: setToken,
      username: username,
      setUsername: setUsername,
      userId: userId,
      setUserId: setUserId
      }}>
      <Router>
        <Route path="/reset_password" component={ResetPassword}/>
        <Route exact path="/" component={Home}/>
        <Route path="/login" component={Login}/>
        <Route path="/registration" component={Registration}/>
        <PrivateRoute path="/test" component={Test}/>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;