# Management system

## Project purpose

The goal of the project is to create a system that will allow central management of users, systems and access to these systems.
The main reason for the existence of such a system is the mitigation of the risk of access to sensitive data, key applications and the automation of giving access to other systems in the organization from one application.

## How to run (for Ubuntu 20.04 LTS):
1. Install docker:
    ```
    sudo apt install docker.io
    sudo systemctl enable --now docker
    ```
2. Install dokcer-compose:
    ```
    sudo apt install docker-compose
    ```
3. Build docker containers:
    ```
    sudo docker-compose up --build
    ```
4. When the containers are built press `CTRL + C`
5. Create relationships in the database: </br>
    ```
    sudo docker-compose run backend sh -c "python manage.py makemigrations" 
    sudo docker-compose run backend sh -c "python manage.py migrate"
    ```
6. Run application: </br>
    ```
    sudo docker-compose up
    ```
7. Hosts (Run in web browser): </br>
    - `localhost:5000` - PGADMIN4 (view of all database relations) </br>
        - EMAIL: `example@email.com` </br>
        - PASSWORD: `admin`
    - `localhost:8000` - DJANGO REST API (view end points)
        - `End points:`
            - `login -> `http://localhost:8000/api-auth/login/
            - `registration -> `http://localhost:8000/api-auth/registration/
            - `reset_password -> `http://localhost:8000/api-auth/reset_password
            - `roles -> `http://localhost:8000/api-permissions/roles/,
            - `toxicRoles -> `http://localhost:8000/api-permissions/toxic_roles/,
            - `userRoles -> `http://localhost:8000/api-permissions/user_roles/,
            - `departaments -> `http://localhost:8000/api-permissions/departaments/,
            - `rolesDepartaments -> `http://localhost:8000/api-permissions/roles_departaments/,
            - `applications -> `http://localhost:8000/api-permissions/applications/,
            - `permissions -> `http://localhost:8000/api-permissions/permissions/
    - `localhost:3000` - React APP (frontend)
8. Run tests:
    ```
    sudo docker-compose run backend sh -c "python -m pytest tests -xvv"
    ```
## How to read documentations:
1. enter the `backend` folder
2. enter the `docs` folder
3. enter the `_build` folder
4. enter the `html` folder
5. open `index.html` file
